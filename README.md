---
pagetitle: gross.css -- Style in just 144 chars
author: Toon Claes
keywords:
  - css
  - minimal
  - framework
  - accessibility
---

# gross.css

CSS file that makes your webpage look good with just 144 bytes.

## Usage

Drop [`gross.css`](gross.css) somewhere in your project and use it in
the html `<head>`:

```html
<link rel="stylesheet" media="all" href="gross.css" />
```

## Motivation

Browser default styles are great, they take care of most of the
styling, they use system fonts, they respect accessibility settings,
etc. But they are pretty ugly. It looks like your visiting a page from
90's. With these 144 characters of CSS you get most of the browser
defaults, with a tiny set of tweaks to make the page look more modern.

## Demo

Visit the [Demo](demo/index.html) to see all the styles. And make sure to have a
look at it in different browsers.

## Features

### Background

Make the background yellowish to make it more calming for the eyes.

### Font

Use system font, but increase the size and the line height.

### Tables

Show horizontal borders and make the table full width.

## FAQ

### Why gross?

12 times a dozen is 144, and it is called a gross. So this it's a
joke. Browsers default styles are ugly, and this CSS file uses just
144 bytes to make it _gross_.

### But why?

Because I can. I wanted to create a ~~small~~tiny project that I
actually was able to get to a completed state. With just 144
characters, I didn't set the bar high.

### How was it started?

It orginally started with a
[tweet](https://twitter.com/pjaspers/status/951754518340997120) by
[\@pjaspers](https://twitter.com/pjaspers):

> The minimal amount of CSS to make a plain old HTML document look
> good? [codepen.io/anon/pen/RxymdE](https://codepen.io/anon/pen/RxymdE)

This got my thinking, and suddenly I came up with the name gross and
the number 144, and I just had to make it happen.

## Changelog

Read the [CHANGELOG](CHANGELOG.md).

## License

gross.css is licensed under [MPL 2.0](LICENSE.txt).

---
pagetitle: gross.css --- Changelog
author: Toon Claes
keywords:
  - css
  - minimal
  - framework
  - accessibility
---

### Version 1.0.0 - Omelette

Initial release, codename Omelette.

Notable features:

- yellowish background
- increased font size and line height
- horizontal borders in tables and full width

all: test public/demo

test: size-check

web: public/index.html public/CHANGELOG.html public/LICENSE.txt

size-check:
	@echo "Filesize: $(size)"
	@test $(size) -eq 144

public/demo: demo.html
	$(MKDIR) -p $@
	$(CP) $< $@/index.html
	$(CP) gross.css $@

public/index.html: README.md

public/%.html:
	$(MKDIR) -p $(dir $@)
	$(CP) gross.css $(dir $@)
	$(PANDOC) -f markdown -t html5 -s -c gross.css -o $@ $(or $<,$(patsubst %.html,%.md,$(notdir $@)))

public/%: %
	$(MKDIR) -p $(dir $@)
	$(CP) $< $@

clean:
	$(RM) -rf public

.PHONY: all test web size-check clean

CP ?= cp
GREP ?= grep
MKDIR ?= mkdir
PANDOC ?= pandoc --no-highlight --lua-filter=pandoc/links-to-markdown.lua
RM ?= rm
WC ?= wc

size = $(strip $(shell $(WC) -c < gross.css))
